<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FrontendController@index')->name('index');
Auth::routes();

Route::match(['get', 'post'], '/admin-login', 'AdminController@login')->name('admin.login');

//Password reset routes
Route::post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset/{token}','Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/admin/dashboard', 'AdminController@dashboard')->name('dashboard');
    Route::get('/admin/profile/{id}', 'AdminController@profile')->name('profile');
    Route::post('/admin/update/profile/{id}', 'AdminController@updateProfile')->name('update.profile');
    Route::match(['get', 'post'], '/admin/edit/password', 'AdminController@editPassword')->name('edit.password');
    Route::post('/admin/edit/check-password', 'AdminController@chkUserPassword');

    	
});


Route::get('/home', 'HomeController@index')->name('home');

Route::get('/logout', 'AdminController@logout')->name('admin.logout');
